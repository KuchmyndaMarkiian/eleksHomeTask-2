﻿/*
 * SonarQube, open source software quality management tool.
 * Copyright (C) 2008-2013 SonarSource
 * mailto:contact AT sonarsource DOT com
 *
 * SonarQube is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * SonarQube is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

using System.Collections.Generic;
using Library.XMLClass;
using static System.Console;

namespace XmlLibrary
{
    static class Program
    {
        static void Main()
        {
            XmlEditor editor;
            editor = new XmlEditor();
            editor.PrintNodes();

            WriteLine("Append child (example: book \"Painted Fox(1-2)\"(Franko) with 67 pages into \"ukrainian\"\")");
            editor.PushNode("Author",new[] {new KeyValuePair<string, string>("name","Franko") },"ukrainian");
            editor.PushNode("Book", new[]
            { 
                new KeyValuePair<string, string>("name", "Painted Fox"),
                 new KeyValuePair<string, string>("pages", "67")
            }, "Franko");
            editor.PushNode("Book", new[]
            {
                new KeyValuePair<string, string>("name", "Painted Fox2"),
                 new KeyValuePair<string, string>("pages", "67")
            }, "Franko");
            editor.PrintNodes();

            ReadKey();

            WriteLine("Remove Author Oscar Wilde");
            editor.RemoveNode("Oscar Wilde");
            editor.PrintNodes();

            ReadKey();

            WriteLine("Replace Painted Fox2 to New");
            editor.ReplaceNode(new[]
            {
                new KeyValuePair<string, string>("name", "New"),
                 new KeyValuePair<string, string>("pages", "78")
            }, "Painted Fox2");
            editor.PrintNodes();
            ReadKey();
            editor.Save();
            ReadKey();
        }
    }
}
