﻿using System.Collections.Generic;
using System.Linq;
using HT1.Datas;
using HT1.Interfaces;

namespace Other
{
    public  static class Extension
    {
        public static string toString(this List<Book> array)
            => array.Select(x => x.ToString()).Aggregate((f, s) => $"{f}\n{s}");
        public static string toString(this List<Author> array)
            => array.Select(x => x.ToString()).Aggregate((f, s) => $"{f}\n{s}");
        public static string toString(this List<Section> array)
             => array.Select(x => x.ToString()).Aggregate((f, s) => $"{f}\n{s}");
    }
}
