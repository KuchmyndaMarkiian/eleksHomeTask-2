using System;
using System.Collections.Generic;
using System.Linq;
using HT1.Files;
using HT1.Interfaces;
using Other;

namespace HT1.Datas
{
    public class Section : ISection, IComparable<Section>
    {
        public string Name { get; set; }

        public List<Author> Authors { get; set; }

        public Section()
        {
        }

        public Section(string name, List<Author> authors)
        {
            this.Name = name;
            this.Authors = authors;
        }

        public override string ToString() => $"Section {Name} which contains books such as:\n{Authors.toString()}\n";
        public int GetBookCount() => Authors.Select(a => a.Books.Count).Sum();

        public int CompareTo(Section other)
        {
            if (GetBookCount() > other.GetBookCount())
                return 1;
            if (GetBookCount() < other.GetBookCount())
                return -1;
            return 0;
        }
    }
}