﻿using System.Collections.Generic;
using HT1.Files;
using Other;

namespace HT1.Datas
{
    class Library : AbstractLibrary
    {
        public Library() :
            base()
        {
        }

        public Library(string name, List<Section> sections) : base(name, sections)
        {
        }

        public override string ToString() => $"Library \"{Name}\" with sections::\n\n{Sections.toString()}\n";
    }
}
